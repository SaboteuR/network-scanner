Network scanner
===============

1. install [virtualbox](https://www.virtualbox.org/);
2. install [vagrant](https://www.vagrantup.com/);
3. run `vagrant up` in project root and wait around 20 minutes. Vagrant will download debian/jessie64 image and set up it to run symfony project (see Vagrantfile). It requires at least 512MB of ram to run;
4. add task via web interface at http://scanner.local.saboteur.me (test:test) ;
5. to start scanning run `vagrant ssh` in project root to log in to virtual machine. Then run `sudo -u www-data /var/www/network-scanner/bin/console task:process --env=dev -vvv`;
6. after that you can see output in web interface;
7. to destroy virtual machine run `vagrant destroy`. This will clean up used disk space.
