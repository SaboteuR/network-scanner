# -*- mode: ruby -*-
# vi: set ft=ruby :

$script =<<SCRIPT
export DEBIAN_FRONTEND=noninteractive

sh -c "cat > /etc/apt/sources.list" <<-EOF
deb http://httpredir.debian.org/debian jessie main
deb http://security.debian.org/ jessie/updates main
deb http://httpredir.debian.org/debian jessie-updates main
EOF

sh -c "cat > /etc/apt/apt.conf.d/99no-recommends" <<-EOF
APT::Install-Recommends "false";
EOF

sh -c "cat > /etc/apt/apt.conf.d/99no-install-suggests" <<-EOF
APT::Install-Suggests "false";
EOF

apt-get -y update

which nmap 2>&1 > /dev/null || apt-get install -y nmap

which php 2>&1 > /dev/null || apt-get install -y php5-fpm php5-mysql

which mysql 2>&1 > /dev/null || apt-get install -y mysql-server

which composer 2>&1 > /dev/null || (curl -sL https://getcomposer.org/composer.phar > composer && chmod +x ./composer && mv ./composer /usr/local/bin/)
SCRIPT

$setupApplication =<<'SCRIPT'
mysqladmin create symfony
cd /var/www/network-scanner \
&& composer install -n \
&& bin/console doctrine:schema:update -n --force \
&& bin/console doctrine:fixtures:load -n \
&& bin/console cache:warmup --env=prod
SCRIPT

$setupNginx =<<SCRIPT
which nginx 2>&1 > /dev/null || apt-get install -y nginx

sh -c "cat > /etc/nginx/sites-enabled/default" <<-'EOF'
server {
  listen *:80 ;
  server_name           scanner.local.saboteur.me;

  index  index.html index.htm index.php;

  access_log            /var/log/nginx/access_scanner.log combined;
  error_log             /var/log/nginx/error_scanner.log;

  location ~ ^/(app|app_dev)\.php(/|$) {
    root          /var/www/network-scanner/web;
    include       /etc/nginx/fastcgi_params;

    fastcgi_pass unix:/var/run/php5-fpm.sock;
    fastcgi_connect_timeout 15;
    fastcgi_index app.php;
    fastcgi_param DOCUMENT_ROOT $realpath_root;
    fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
    fastcgi_read_timeout 120;
    fastcgi_send_timeout 120;
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
  }

  location / {
    root      /var/www/network-scanner/web;
    try_files $uri /app.php$is_args$args;
  }
}
EOF

service nginx reload

SCRIPT

Vagrant.configure(2) do |config|
  config.vm.box = "debian/jessie64"

  config.vm.network 'private_network', ip: '192.168.62.33'
  config.vm.provider "virtualbox" do |vb|
    # Use VBoxManage to customize the VM.
    vb.customize ["modifyvm", :id, "--memory", "512"]
    vb.customize ["modifyvm", :id, "--cpus", "1"]
    vb.customize ['modifyvm', :id, '--name', config.vm.hostname]
  end

  config.vm.provision "set up vbox environment", type: "shell", run: "once", inline: $script
  config.vm.provision "set up nginx host", type: "shell", run: "once", inline: $setupNginx
  config.vm.provision "set up application", type: "shell", run: "once", inline: $setupApplication

  config.vm.synced_folder ".", "/var/www/network-scanner", :owner => "www-data"
end
