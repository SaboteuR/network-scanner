<?php

namespace NetworkScanner\Enum;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
final class TaskState
{
    const CREATED = 'created';
    const RUNNING = 'running';
    const SUCCESSFUL = 'success';
    const FAILED = 'failed';
}
