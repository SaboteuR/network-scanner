<?php

namespace NetworkScanner\Model;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class Task
{
    /** @var int */
    protected $id;
    /** @var string */
    protected $title;
    /** @var User */
    protected $user;
    /** @var string */
    protected $executable;
    /** @var array */
    protected $systemParameters = [];
    /** @var array */
    protected $userParameters = [];
    /** @var float */
    protected $progress = 0;
    /** @var string */
    protected $state;
    /** @var boolean */
    protected $locked = false;
    /** @var array */
    protected $result = [];
    /** @var array */
    protected $progressParserParameters = [];
    /** @var array */
    protected $resultParserParameters = [];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getExecutable()
    {
        return $this->executable;
    }

    /**
     * @param string $executable
     */
    public function setExecutable($executable)
    {
        $this->executable = $executable;
    }

    /**
     * @return array
     */
    public function getSystemParameters()
    {
        return $this->systemParameters;
    }

    /**
     * @param array $systemParameters
     */
    public function setSystemParameters($systemParameters)
    {
        $this->systemParameters = $systemParameters;
    }

    /**
     * @return array
     */
    public function getUserParameters()
    {
        return $this->userParameters;
    }

    /**
     * @param array $userParameters
     */
    public function setUserParameters($userParameters)
    {
        $this->userParameters = $userParameters;
    }

    /**
     * @return float
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * @param float $progress
     */
    public function setProgress($progress)
    {
        $this->progress = $progress;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return boolean
     */
    public function isLocked()
    {
        return $this->locked;
    }

    /**
     * @param boolean $locked
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param array $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return array
     */
    public function getProgressParserParameters()
    {
        return $this->progressParserParameters;
    }

    /**
     * @param array $progressParserParameters
     */
    public function setProgressParserParameters($progressParserParameters)
    {
        $this->progressParserParameters = $progressParserParameters;
    }

    /**
     * @return array
     */
    public function getResultParserParameters()
    {
        return $this->resultParserParameters;
    }

    /**
     * @param array $resultParserParameters
     */
    public function setResultParserParameters($resultParserParameters)
    {
        $this->resultParserParameters = $resultParserParameters;
    }
}
