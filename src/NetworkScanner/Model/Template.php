<?php

namespace NetworkScanner\Model;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class Template
{
    /** @var int */
    protected $id;
    /** @var string */
    protected $title;
    /** @var string */
    protected $description = '';
    /** @var string */
    protected $executable;
    /** @var array */
    protected $systemParameters = [];
    /** @var array */
    protected $configurableParameters = [];
    /** @var array */
    protected $progressParserParameters = [];
    /** @var array */
    protected $resultParserParameters = [];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getExecutable()
    {
        return $this->executable;
    }

    /**
     * @param string $executable
     */
    public function setExecutable($executable)
    {
        $this->executable = $executable;
    }

    /**
     * @return array
     */
    public function getSystemParameters()
    {
        return $this->systemParameters;
    }

    /**
     * @param array $systemParameters
     */
    public function setSystemParameters($systemParameters)
    {
        $this->systemParameters = $systemParameters;
    }

    /**
     * @return array
     */
    public function getConfigurableParameters()
    {
        return $this->configurableParameters;
    }

    /**
     * @param array $configurableParameters
     */
    public function setConfigurableParameters($configurableParameters)
    {
        $this->configurableParameters = $configurableParameters;
    }

    /**
     * @return array
     */
    public function getProgressParserParameters()
    {
        return $this->progressParserParameters;
    }

    /**
     * @param array $progressParserParameters
     */
    public function setProgressParserParameters($progressParserParameters)
    {
        $this->progressParserParameters = $progressParserParameters;
    }

    /**
     * @return array
     */
    public function getResultParserParameters()
    {
        return $this->resultParserParameters;
    }

    /**
     * @param array $resultParserParameters
     */
    public function setResultParserParameters($resultParserParameters)
    {
        $this->resultParserParameters = $resultParserParameters;
    }
}
