<?php

namespace NetworkScanner\Model;

use FOS\UserBundle\Model\User as BaseUser;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class User extends BaseUser
{

}
