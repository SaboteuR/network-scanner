<?php

namespace NetworkScanner\Repository;

use NetworkScanner\Model\Task;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
interface TaskRepositoryInterface extends ObjectRepositoryInterface
{
    /**
     * @return Task
     */
    public function create();

    /**
     * @param Task $task
     */
    public function persist(Task $task);
}
