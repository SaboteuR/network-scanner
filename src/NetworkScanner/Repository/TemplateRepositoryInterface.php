<?php

namespace NetworkScanner\Repository;

use NetworkScanner\Model\Template;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
interface TemplateRepositoryInterface extends ObjectRepositoryInterface
{
    /**
     * @return Template
     */
    public function create();

    /**
     * @param Template $template
     */
    public function persist(Template $template);
}
