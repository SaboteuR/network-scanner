<?php

namespace NetworkScanner\Service\Job;

use NetworkScanner\Service\Process\ProcessRunner;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class JobExecutorService
{
    /** @var ProcessRunner */
    private $processRunner;
}
