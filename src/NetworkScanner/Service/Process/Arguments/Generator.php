<?php

namespace NetworkScanner\Service\Process\Arguments;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class Generator
{
    /**
     * @param array $systemParameters
     * @param array $userParameters
     * @return array
     */
    public function generate(array $systemParameters, array $userParameters)
    {
        $parameters = [];
        $appended = [];

        $merged = array_merge($userParameters, $systemParameters);
        foreach ($merged as $key => $value) {
            if(is_int($key)) {
                $appended[] = $value;
            } else {
                $parameters[] = $key;
                $parameters[] = $value;
            }
        }
        
        return array_merge($parameters, $appended);
    }
}
