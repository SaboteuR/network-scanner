<?php

namespace NetworkScanner\Service\Process;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\ProcessBuilder;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class ProcessFactory
{
    /** @var ProcessBuilder */
    private $processBuilder;
    /** @var int */
    private $defaultIdleTimeout = 4;
    /** @var int|null */
    private $defaultProcessTimeout = null;

    /**
     * @param ProcessBuilder $processBuilder
     */
    public function __construct(ProcessBuilder $processBuilder)
    {
        $this->processBuilder = $processBuilder;
    }

    /**
     * @param string $executable
     * @param array $arguments
     * 
     * @return Process
     */
    public function create($executable, array $arguments)
    {
        $this->processBuilder->setPrefix($executable);
        $process = $this->processBuilder->setArguments($arguments)->getProcess();
        $process->setTimeout($this->defaultProcessTimeout);
        $process->setIdleTimeout($this->defaultIdleTimeout);
        
        return $process;
    }
}
