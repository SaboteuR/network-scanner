<?php

namespace NetworkScanner\Service\Process;

use NetworkScanner\Enum\TaskState;
use NetworkScanner\Model\Task;
use NetworkScanner\Service\Process\Arguments\Generator;
use NetworkScanner\Service\Task\TaskResultService;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Process\Exception\LogicException;
use Symfony\Component\Process\Exception\RuntimeException;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class ProcessRunner
{
    /** @var ProcessFactory */
    private $processFactory;
    /** @var ProgressReporterFactory */
    private $progressReporterFactory;
    /** @var ResultParser */
    private $resultParser;
    /** @var Generator */
    private $argumentGenerator;
    /** @var TaskResultService */
    private $taskResultService;
    /** @var LoggerInterface */
    private $logger;

    /**
     * @param ProcessFactory $processFactory
     * @param ProgressReporterFactory $progressReporterFactory
     * @param ResultParser $resultParser
     * @param Generator $argumentGenerator
     * @param TaskResultService $taskResultService
     * @param LoggerInterface|null $logger
     */
    public function __construct(
        ProcessFactory $processFactory,
        ProgressReporterFactory $progressReporterFactory,
        ResultParser $resultParser,
        Generator $argumentGenerator,
        TaskResultService $taskResultService,
        LoggerInterface $logger = null
    ) {
        $this->processFactory = $processFactory;
        $this->progressReporterFactory = $progressReporterFactory;
        $this->resultParser = $resultParser;
        $this->argumentGenerator = $argumentGenerator;
        $this->taskResultService = $taskResultService;
        $this->logger = $logger ?: new NullLogger();
    }

    /**
     * Runs given scan task
     *
     * @param Task $task
     */
    public function run(Task $task)
    {
        $arguments = $this->argumentGenerator->generate($task->getSystemParameters(), $task->getUserParameters());
        $process = $this->processFactory->create($task->getExecutable(), $arguments);
        $progressReporter = $this->progressReporterFactory->createForTask($task);

        //TODO check if locked and check if not finished yet
        try {
            $this->taskResultService->setResult($task, TaskState::RUNNING, []);
            $this->logger->debug(
                sprintf(
                    'Task %d: running %s %s',
                    $task->getId(),
                    $task->getExecutable(),
                    implode(' ', $arguments)
                )
            );

            $process->run(
                function ($type, $buffer) use ($progressReporter) {
                    $progressReporter->parseProgress($type, $buffer);
                }
            );

            if($process->isSuccessful()) {
                $result = $this->resultParser->parse($task->getResultParserParameters(), $process->getOutput());
                $this->taskResultService->setResult($task, TaskState::SUCCESSFUL, $result);
            } else {
                $this->taskResultService->setResult($task, TaskState::FAILED, []);
            }
        } catch (RuntimeException $e) {
            $this->taskResultService->setResult($task, TaskState::FAILED, []);
        } catch (LogicException $ex) {
            $this->taskResultService->setResult($task, TaskState::FAILED, []);
        }
        //TODO set human readable message that something had failed

        $progressReporter->reportProgress(100);
    }
}
