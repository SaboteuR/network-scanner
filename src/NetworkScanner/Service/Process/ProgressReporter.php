<?php

namespace NetworkScanner\Service\Process;

use NetworkScanner\Model\Task;
use NetworkScanner\Service\Task\TaskProgressService;
use Symfony\Component\Process\Process;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class ProgressReporter
{
    /** @var TaskProgressService */
    private $progressService;
    /** @var Task */
    private $task;
    /** @var string */
    private $parserParameter;

    /**
     * @param TaskProgressService $progressService
     * @param Task $task
     */
    public function __construct(TaskProgressService $progressService, Task $task)
    {
        $this->progressService = $progressService;
        $this->task = $task;
        $this->parserParameter = current($task->getResultParserParameters());
    }

    /**
     * Parses in-progress output of some command
     *
     * @param string $type
     * @param string $buffer
     */
    public function parseProgress($type, $buffer)
    {
        if (Process::ERR === $type) {
            return;
        }

        if(1 === preg_match($this->parserParameter, $buffer, $matches)) {
            $progress = (float)$matches[1];
            $this->reportProgress($progress);
        }
    }

    /**
     * Reports task progress to database
     *
     * @param float $progress
     */
    public function reportProgress($progress)
    {
        $this->progressService->setProgress($this->task, $progress);
    }
}
