<?php

namespace NetworkScanner\Service\Process;

use NetworkScanner\Model\Task;
use NetworkScanner\Service\Task\TaskProgressService;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class ProgressReporterFactory
{
    /** @var TaskProgressService */
    private $progressService;

    /**
     * ProgressReporterFactory constructor.
     * @param TaskProgressService $progressService
     */
    public function __construct(TaskProgressService $progressService)
    {
        $this->progressService = $progressService;
    }

    public function createForTask(Task $task)
    {
        return new ProgressReporter($this->progressService, $task);
    }
}
