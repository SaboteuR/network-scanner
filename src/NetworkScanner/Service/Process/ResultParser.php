<?php

namespace NetworkScanner\Service\Process;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class ResultParser
{
    /**
     * Parse process output according configured parsing rules
     * 
     * @param array $parseConfig
     * @param string $processOutput
     * @return array
     */
    public function parse(array $parseConfig, $processOutput)
    {
        $result = [];
        foreach ($parseConfig as $fieldName => $parseRegex) {
            if(1 === preg_match($parseRegex, $processOutput, $matches)) {
                $result[$fieldName] = $matches[1];
            }
        }
        
        return $result;
    }
}
