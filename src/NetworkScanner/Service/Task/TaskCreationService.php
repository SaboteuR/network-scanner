<?php

namespace NetworkScanner\Service\Task;

use NetworkScanner\Enum\TaskState;
use NetworkScanner\Model\Task;
use NetworkScanner\Model\Template;
use NetworkScanner\Model\User;
use NetworkScanner\Repository\TaskRepositoryInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class TaskCreationService
{
    /** @var TaskRepositoryInterface */
    private $repository;

    /**
     * @param TaskRepositoryInterface $repository
     */
    public function __construct(TaskRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Creates new task
     * @return Task
     */
    public function create()
    {
        $task = $this->repository->create();
        $task->setProgress(0);
        $task->setState(TaskState::CREATED);
        $task->setLocked(false);
        //todo create from template?
        return $task;
    }

    /**
     * Creates new Task from Template and parameters provided by user
     *
     * @param Template $template
     * @param array $userParameters
     *
     * @return Task
     */
    public function createFromTemplate(Template $template, User $user, array $userParameters)
    {
        $task = $this->create();
        $task->setSystemParameters($template->getSystemParameters());
        $task->setExecutable($template->getExecutable());
        $task->setProgressParserParameters($template->getProgressParserParameters());
        $task->setResultParserParameters($template->getResultParserParameters());
        $task->setUser($user);

        //Move title calculation to separate service
        $title = sprintf('%s: %s', $template->getTitle(), date('Y.m.d H:i:s'));
        $task->setTitle($title);


        //Move this to separate service
        $target = $userParameters['target'];
        unset($userParameters['target']);
        $parameters = [];
        $templateConfig = $template->getConfigurableParameters();
        foreach ($userParameters as $parameter => $value) {
            $commandLineParameter = $templateConfig[$parameter]['parameter'];
            $parameters[$commandLineParameter] = $value;
        }
        $parameters[] = $target;

        $task->setUserParameters($parameters);

        return $task;
    }

    /**
     * Persists task
     * @param Task $task
     */
    public function persist(Task $task)
    {
        $this->repository->persist($task);
    }
}
