<?php

namespace NetworkScanner\Service\Task;

use NetworkScanner\Model\Task;
use NetworkScanner\Repository\TaskRepositoryInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class TaskListService
{
    /** @var TaskRepositoryInterface */
    private $repository;

    /**
     * TaskListService constructor.
     * @param TaskRepositoryInterface $repository
     */
    public function __construct(TaskRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }
    
    /**
     * @param int $limit
     * @param int $offset
     * @return Task[]
     */
    public function get($limit = 10, $offset = 0)
    {
        return $this->repository->findBy([], ['id' => 'DESC'], $limit, $offset);
    }
}
