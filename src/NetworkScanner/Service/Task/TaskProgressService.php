<?php

namespace NetworkScanner\Service\Task;

use NetworkScanner\Model\Task;
use NetworkScanner\Repository\TaskRepositoryInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class TaskProgressService
{
    /** @var TaskRepositoryInterface */
    private $repository;
    /** @var LoggerInterface */
    private $logger;

    /**
     * TaskProgressService constructor.
     * @param TaskRepositoryInterface $repository
     * @param LoggerInterface $logger
     */
    public function __construct(
        TaskRepositoryInterface $repository,
        LoggerInterface $logger
    ) {
        $this->repository = $repository;
        $this->logger = $logger ?: new NullLogger();
    }

    public function setProgress(Task $task, $progress)
    {
        $this->logger->debug(
            sprintf('Setting progress of task %d to %.2f', $task->getId(), $progress)
        );

        $task->setProgress($progress);

        $this->repository->persist($task);
    }
}
