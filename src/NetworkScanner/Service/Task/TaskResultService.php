<?php

namespace NetworkScanner\Service\Task;

use NetworkScanner\Model\Task;
use NetworkScanner\Repository\TaskRepositoryInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class TaskResultService
{
    /** @var TaskRepositoryInterface */
    private $repository;
    /** @var LoggerInterface */
    private $logger;

    /**
     * @param TaskRepositoryInterface $repository
     * @param LoggerInterface $logger
     */
    public function __construct(
        TaskRepositoryInterface $repository,
        LoggerInterface $logger = null
    ) {
        $this->repository = $repository;
        $this->logger = $logger ?: new NullLogger();
    }

    public function setResult(Task $task, $state, array $result)
    {
        $this->logger->debug(
            sprintf('Setting state %s for task %d', $state, $task->getId()),
            ['result' => $result]
        );
        $task->setState($state);
        $task->setResult($result);

        $this->repository->persist($task);
    }
}
