<?php

namespace NetworkScanner\Service\Task;

use NetworkScanner\Model\Task;
use NetworkScanner\Repository\TaskRepositoryInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class TaskService
{
    /** @var TaskRepositoryInterface */
    private $repository;

    /**
     * TaskService constructor.
     * @param TaskRepositoryInterface $repository
     */
    public function __construct(TaskRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Finds an object by its primary key / identifier.
     *
     * @param mixed $id The identifier.
     *
     * @return Task The object.
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Finds a single object by a set of criteria.
     *
     * @param array $criteria The criteria.
     *
     * @return Task The object.
     */
    public function findOneBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }
}
