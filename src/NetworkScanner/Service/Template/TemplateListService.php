<?php

namespace NetworkScanner\Service\Template;

use NetworkScanner\Model\Template;
use NetworkScanner\Repository\TemplateRepositoryInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class TemplateListService
{
    /** @var TemplateRepositoryInterface */
    private $repository;

    /**
     * TemplateListService constructor.
     * @param TemplateRepositoryInterface $repository
     */
    public function __construct(TemplateRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return Template[]
     */
    public function get($limit = 10, $offset = 0)
    {
        return $this->repository->findBy([], ['id' => 'DESC'], $limit, $offset);
    }
}
