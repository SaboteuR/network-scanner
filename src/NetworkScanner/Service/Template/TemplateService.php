<?php

namespace NetworkScanner\Service\Template;

use NetworkScanner\Model\Template;
use NetworkScanner\Repository\TemplateRepositoryInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class TemplateService
{
    /** @var TemplateRepositoryInterface */
    private $repository;

    /**
     * TemplateService constructor.
     * @param TemplateRepositoryInterface $repository
     */
    public function __construct(TemplateRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Finds an object by its primary key / identifier.
     *
     * @param mixed $id The identifier.
     *
     * @return Template The object.
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Finds a single object by a set of criteria.
     *
     * @param array $criteria The criteria.
     *
     * @return Template The object.
     */
    public function findOneBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }
}
