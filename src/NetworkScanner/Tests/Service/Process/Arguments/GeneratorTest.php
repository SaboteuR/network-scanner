<?php

namespace NetworkScanner\Tests\Service\Process\Arguments;

use NetworkScanner\Service\Process\Arguments\Generator;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class GeneratorTest extends \PHPUnit_Framework_TestCase
{
    /** @var Generator */
    private $subject;

    protected function setUp()
    {
        $this->subject = new Generator();
    }

    /**
     * @test
     */
    public function it_generates_flat_array_out_of_system_and_user_parameters()
    {
        $userParameters = [
            '--script'      => 'ssl-heartbleed',
            '--script-args' => 'vulns.showall',
            '-p'            => '443',
            'saboteur.me'
        ];
        $systemParameters = [
            '--stats-every' => '3s',
            '-p'            => '80'
        ];

        $expectation = ['--script', 'ssl-heartbleed', '--script-args', 'vulns.showall', '-p', '80', '--stats-every', '3s', 'saboteur.me'];

        $result = $this->subject->generate($systemParameters, $userParameters);
        $this->assertEquals($expectation, $result);
    }
}
