<?php

namespace NetworkScanner\Tests\Service\Process;

use NetworkScanner\Service\Process\ResultParser;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class ResultParserTest extends \PHPUnit_Framework_TestCase
{
    /** @var ResultParser */
    private $subject;

    protected function setUp()
    {
        $this->subject = new ResultParser();
    }

    /**
     * @test
     */
    public function it_parses_process_output_with_regular_expressions()
    {
        $sample =<<<SAMPLE
Starting Nmap 6.47 ( http://nmap.org ) at 2016-06-05 12:17 GMT
Nmap scan report for api.sipgate.net (217.10.73.254)
Host is up (0.023s latency).
PORT    STATE SERVICE
443/tcp open  https
| ssl-heartbleed:
|   NOT VULNERABLE:
|   The Heartbleed Bug is a serious vulnerability in the popular OpenSSL cryptographic software library. It allows for stealing information intended to be protected by SSL/TLS encryption.
|     State: NOT VULNERABLE
|     References:
|       http://www.openssl.org/news/secadv_20140407.txt
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0160
|_      http://cvedetails.com/cve/2014-0160/

Nmap done: 1 IP address (1 host up) scanned in 1.62 seconds
SAMPLE;

        $parseConfig = [
            'state' => '/State: (NOT VULNERABLE|VULNERABLE)/',
        ];

        $result = $this->subject->parse($parseConfig, $sample);

        $this->assertEquals(['state' => 'NOT VULNERABLE'], $result);
    }
}
