<?php

namespace NetworkScannerBundle\Command;

use NetworkScanner\Enum\TaskState;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class ProcessTaskCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('task:process')
            ->setDescription('Test command to launch task processing')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $taskService = $this->getContainer()->get('task.service.list');
        $tasks = $taskService->get(100, 0);
        foreach ($tasks as $task) {
            //This is test command so we can use php filtering instead of db query
            if(TaskState::CREATED !== $task->getState()) {
                continue;
            }
            $processRunner = $this->getContainer()->get('process.service.runner');
            $processRunner->run($task);
        }
    }
}
