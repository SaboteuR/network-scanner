<?php

namespace NetworkScannerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function dashboardAction()
    {
        $dashbord = $this->get('network_scanner.service.dashboard')->get();
        
        return $this->render(
            'NetworkScannerBundle:Default:index.html.twig',
            [
                'dashboard' => $dashbord
            ]
        );
    }

    public function indexAction()
    {
        return $this->redirectToRoute('dashboard');
    }
}
