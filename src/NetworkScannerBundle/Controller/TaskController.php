<?php

namespace NetworkScannerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class TaskController extends Controller
{
    public function detailsAction($id)
    {
        $task = $this->get('network_scanner.service.task_details')->get($id);
        if(!$task) {
            throw $this->createNotFoundException();
        }

        return $this->render(
            'NetworkScannerBundle:Task:details.html.twig',
            [
                'task' => $task
            ]
        );
    }

    public function newAction($templateId, Request $request)
    {
        $template = $this->get('template.service.finder')->find($templateId);
        if(!$template) {
            throw $this->createNotFoundException();
        }

        $form = $this->get('network_scanner.form.task_parameters_form_builder')->buildFromTemplate($template);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // ... perform some action, such as saving the task to the database
            $creationService = $this->get('task.service.creation');
            $task = $creationService->createFromTemplate($template, $this->getUser(), $form->getData());
            $creationService->persist($task);
            
            return $this->redirectToRoute('dashboard');
        }

        return $this->render(
            'NetworkScannerBundle:Task:new.html.twig',
            [
                'template' => $template,
                'form'     => $form->createView()
            ]
        );
    }
}
