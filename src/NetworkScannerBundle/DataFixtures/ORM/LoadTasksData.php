<?php

namespace NetworkScannerBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NetworkScanner\Service\Task\TaskCreationService;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class LoadTasksData implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function load(ObjectManager $manager)
    {
        return;
        
        /** @var TaskCreationService $creationService */
        $creationService = $this->container->get('task.service.creation');
        $task = $creationService->create();
        $task->setExecutable('/usr/bin/nmap');
        $task->setSystemParameters([
            '--stats-every' => '3s',
            '--script'      => 'ssl-heartbleed',
            '--script-args' => 'vulns.showall',
        ]);
        $task->setUserParameters([
            '-p'            => '443',
            'saboteur.me'
        ]);
        $task->setProgress(0);
        $task->setProgressParserParameters([
            '/About ([\d\.]+)% done/'
        ]);
        $task->setResultParserParameters([
            'state' => '/State: (NOT VULNERABLE|VULNERABLE)/',
        ]);
        $task->setTitle('Test task');

        $creationService->persist($task);
    }

    public function getOrder()
    {
        return 20;
    }
}
