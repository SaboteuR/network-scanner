<?php

namespace NetworkScannerBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\Range;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class LoadTemplateData implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function load(ObjectManager $manager)
    {
        /** @var ContainerInterface $container */
        $container = $this->container;
        $templateRepository = $container->get('template.repository');
        $template = $templateRepository->create();
        $template->setTitle('heartbleed scan');
        $template->setDescription('scans for famous heartbleed vurnability');
        $template->setExecutable('/usr/bin/nmap');
        $template->setSystemParameters([
            '--stats-every' => '3s',
            '--script'      => 'ssl-heartbleed',
            '--script-args' => 'vulns.showall',
        ]);
        $template->setConfigurableParameters([
            'port'            => [
                'parameter'         => '-p',
                'description'       => 'port',
                'constraint'        => Range::class,
                'constraint_params' => [
                    'min'        => 1,
                    'max'        => 65535,
                    'minMessage' => 'Port should start at {{ limit }}',
                    'maxMessage' => 'Port should not be greater then {{ limit }}',
                ]
            ],
        ]);
        $template->setProgressParserParameters([
            '/About ([\d\.]+)% done/'
        ]);
        $template->setResultParserParameters([
            'state' => '/State: (NOT VULNERABLE|VULNERABLE)/',
        ]);

        $templateRepository->persist($template);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 10;
    }
}
