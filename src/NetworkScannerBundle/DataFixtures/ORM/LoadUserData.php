<?php

namespace NetworkScannerBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NetworkScanner\Repository\TaskRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class LoadUserData implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function load(ObjectManager $manager)
    {
        /** @var ContainerInterface $container */
        $container = $this->container;
        $manipulator = $container->get('fos_user.util.user_manipulator');
        $manipulator->create('test', 'test', 'test@example.com', true, false);
        $manipulator->addRole('test', 'ROLE_ADMIN');
    }

    public function getOrder()
    {
        return 0;
    }
}
