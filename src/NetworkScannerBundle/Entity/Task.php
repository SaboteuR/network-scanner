<?php

namespace NetworkScannerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use NetworkScanner\Model\Task as TaskModel;
use NetworkScannerBundle\Doctrine\Annotation as NS;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */

/**
 * @ORM\Entity(repositoryClass="NetworkScannerBundle\Repository\TaskRepository")
 * @ORM\Table(name="tasks")
 * @NS\UserAware(userFieldName="user_id")
 */
class Task extends TaskModel
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $title;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    protected $user;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $executable;

    /**
     * @ORM\Column(type="json_array", name="system_parameters", nullable=true)
     */
    protected $systemParameters;

    /**
     * @ORM\Column(type="json_array", name="user_parameters", nullable=false)
     */
    protected $userParameters;

    /**
     * @ORM\Column(type="string", nullable=false, options={"default":0})
     */
    protected $progress;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $state;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":false})
     */
    protected $locked;

    /**
     * @ORM\Column(type="json_array", nullable=false)
     */
    protected $result = [];

    /**
     * @ORM\Column(type="json_array", name="progress_parser_parameters", nullable=false)
     */
    protected $progressParserParameters = [];

    /**
     * @ORM\Column(type="json_array", name="result_parser_parameters", nullable=false)
     */
    protected $resultParserParameters = [];
}
