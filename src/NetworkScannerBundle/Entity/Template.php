<?php

namespace NetworkScannerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use NetworkScanner\Model\Template as TemplateModel;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */

/**
 * @ORM\Entity(repositoryClass="NetworkScannerBundle\Repository\TemplateRepository")
 * @ORM\Table(name="templates")
 */
class Template extends TemplateModel
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false, length=64)
     */
    protected $title;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $executable;

    /**
     * @ORM\Column(type="json_array", name="system_parameters", nullable=true)
     */
    protected $systemParameters;

    /**
     * @ORM\Column(type="json_array", name="configurable_parameters", nullable=false)
     */
    protected $configurableParameters;

    /**
     * @ORM\Column(type="json_array", name="progress_parser_parameters", nullable=false)
     */
    protected $progressParserParameters = [];

    /**
     * @ORM\Column(type="json_array", name="result_parser_parameters", nullable=false)
     */
    protected $resultParserParameters = [];
}
