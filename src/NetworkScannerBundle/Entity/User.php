<?php

namespace NetworkScannerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use NetworkScanner\Model\User as UserModel;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends UserModel
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
}
