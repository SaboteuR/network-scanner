<?php

namespace NetworkScannerBundle\Form;

use NetworkScanner\Model\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class TaskParametersFormBuilder
{
    /** @var FormFactoryInterface */
    private $formFactory;

    /**
     * TaskParametersFormBuilder constructor.
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    public function buildFromTemplate(Template $template)
    {
        $builder = $this->formFactory->createBuilder();

        $parameters = $template->getConfigurableParameters();
        foreach ($parameters as $parameter => $config) {
            $options = [
                'label' => $config['description']
            ];
            if(isset($config['constraint'])) {
                $options['constraints'] = [
                    new $config['constraint']($config['constraint_params'])
                ];
            }

            $builder->add($parameter, null, $options);
        }
        $builder->add('target', null);
        $builder->add('submit', SubmitType::class);

        return $builder->getForm();
    }
}
