<?php

namespace NetworkScannerBundle\Model;

use NetworkScanner\Model\Task;
use NetworkScanner\Model\Template;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class Dashboard
{
    /** @var Task[] */
    private $tasks;
    /** @var Template[] */
    private $templates;

    /**
     * Dashboard constructor.
     * @param Task[] $tasks
     * @param Template[] $templates
     */
    public function __construct(array $tasks, array $templates)
    {
        $this->tasks = $tasks;
        $this->templates = $templates;
    }

    /**
     * @return Task[]
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @return Template[]
     */
    public function getTemplates()
    {
        return $this->templates;
    }
}
