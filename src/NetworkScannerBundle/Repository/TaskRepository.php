<?php

namespace NetworkScannerBundle\Repository;

use Doctrine\ORM\EntityRepository;
use NetworkScanner\Model\Task;
use NetworkScanner\Repository\TaskRepositoryInterface;
use NetworkScannerBundle\Entity\Task as TaskEntity;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class TaskRepository extends EntityRepository implements TaskRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function create()
    {
        return new TaskEntity();
    }

    /**
     * @inheritdoc
     */
    public function persist(Task $task)
    {
        $em = $this->getEntityManager();
        $em->persist($task);
        $em->flush();
    }
}
