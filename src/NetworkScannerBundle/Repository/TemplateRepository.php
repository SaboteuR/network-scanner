<?php

namespace NetworkScannerBundle\Repository;

use Doctrine\ORM\EntityRepository;
use NetworkScanner\Model\Template;
use NetworkScanner\Repository\TemplateRepositoryInterface;
use NetworkScannerBundle\Entity\Template as TemplateEntity;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class TemplateRepository extends EntityRepository implements TemplateRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function create()
    {
        return new TemplateEntity();
    }

    /**
     * @inheritdoc
     */
    public function persist(Template $template)
    {
        $em = $this->getEntityManager();
        $em->persist($template);
        $em->flush();
    }
}
