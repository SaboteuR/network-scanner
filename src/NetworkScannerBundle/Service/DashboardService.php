<?php

namespace NetworkScannerBundle\Service;

use NetworkScanner\Service\Task\TaskListService;
use NetworkScanner\Service\Template\TemplateListService;
use NetworkScannerBundle\Model\Dashboard;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class DashboardService
{
    /** @var TemplateListService */
    private $templateListService;
    /** @var TaskListService */
    private $taskListService;

    /**
     * DashboardService constructor.
     * @param TemplateListService $templateListService
     * @param TaskListService $taskListService
     */
    public function __construct(TemplateListService $templateListService, TaskListService $taskListService)
    {
        $this->templateListService = $templateListService;
        $this->taskListService = $taskListService;
    }

    /**
     * Returns model representing dashboard on start page
     * 
     * @return Dashboard
     */
    public function get()
    {
        $tasks = $this->taskListService->get(10, 0);
        $templates = $this->templateListService->get(10, 0);
        
        return new Dashboard($tasks, $templates);
    }
}
