<?php

namespace NetworkScannerBundle\Service;

use NetworkScanner\Service\Task\TaskService;

/**
 * @author Evgeny Soynov<saboteur@saboteur.me>
 * @copyright (C) 2016 Evgeny Soynov. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 */
class TaskDetailsService
{
    /** @var TaskService */
    private $taskService;

    /**
     * TaskDetailsService constructor.
     * @param TaskService $taskService
     */
    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    public function get($id)
    {
        return $this->taskService->find($id);
    }
}
