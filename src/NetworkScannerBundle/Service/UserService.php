<?php

namespace NetworkScannerBundle\Service;

/**
 * @author Evgeny Soynov<evgeny.soynov@sio.ag>
 */
class UserService
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * UserService constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }
    
}
